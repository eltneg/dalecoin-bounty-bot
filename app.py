from flask import request, jsonify
import telegram
from manage import manager, app
from bounty_bot.bot import BOT, webhook
from db import GoogleForm, Tasks, Bounter
"""
@app.route('/')
def home():
    print(request.remote_addr)
    print(request.headers['X-Forwarded-For'])
    return "hello", 200
"""
@app.route('/ggl', methods=['POST', 'GET'])
def ggl():
    """
    Listens to update from google form and
    stores update to database
    """
    if request.method == 'GET':
        return 'ok', 200

    resp = request.get_json(force=True)
    email = resp.get('email')
    telegram_username = resp.get('telegram_username').lower()
    facebook_username = resp.get('facebook_username').lower()
    twitter_username = resp.get('twitter_username').lower()
    eth_addr = resp.get('eth')
    #store to db
    try:
        GoogleForm(
            email,telegram_username,
            facebook_username, twitter_username,
            eth_addr
    )
    #commit
    except Exception as e:
        print(e)

    print(resp)
    return 'ok', 200

@app.route('/tele', methods=['POST', 'GET'])
def telegram_bot():
    """Listens to update from telegram"""
    if request.method == 'GET':
        return 'ok', 200

    if request.method == 'POST':
        update = telegram.Update.de_json(request.get_json(force=True), BOT)
        webhook(update)
        return 'ok', 200

@app.route('/add-new-task', methods=['POST'])
def new_task():
    data = request.get_json(force=True)
    task = data.get('task')
    Tasks(task)
    #commit
    return 'ok', 200

@app.route('/all-tasks')
def all_tasks():
    index = 1
    tasks = Tasks.query.all()
    resp = {}
    for task in tasks:
        resp[str(index)] = task.task_desc
        index += 1

    return jsonify(resp), 200

@app.route('/fb/user', methods=['POST', 'GET'])
def fb():
    if request.method == 'GET':
        challenge = request.args.get('hub.challenge')
        v_token = request.args.get('hub.verify_token')
        return str(challenge)
    print(request.get_json(force=True))
    return 'OK', 200

@app.route('/fb/pages', methods=['POST', 'GET'])
def fb_pages():
    if request.method == 'GET':
        challenge = request.args.get('hub.challenge')
        v_token = request.args.get('hub.verify_token')
        return str(challenge)

    print(request.get_json(force=True))
    return 'OK', 200

if __name__ == '__main__':
    manager.run()
    