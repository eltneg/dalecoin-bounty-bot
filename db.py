from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

db = SQLAlchemy()

class BaseModel(db.Model):
    __abstract__ = True

    created_on = db.Column(
        db.DateTime, nullable=False, default=datetime.utcnow()
    )
    updated_on = db.Column(
        db.DateTime, nullable=False, default=datetime.utcnow(),
        onupdate=datetime.utcnow()
    )

class GoogleForm(BaseModel):
    __Tablename__ = 'Google'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(40), unique=True)
    telegram_username = db.Column(db.String(40), unique=True)
    facebook_username = db.Column(db.String(40), unique=True)
    twitter_username = db.Column(db.String(40), unique=True)
    eth_address = db.Column(db.String(50), unique=True)
    points = db.Column(db.Integer)
    task_completed = db.Column(db.String(500))

    
    def __init__(self, email, telegram_username, facebook_username, twitter_username, eth_address):
        self.email = email
        self.telegram_username = telegram_username
        self.facebook_username = facebook_username
        self.twitter_username = twitter_username
        self.eth_address = eth_address

class Bounter(BaseModel):
    __Tablename__ = "Bounter"
    id = db.Column(db.Integer, primary_key=True)
    telegram_id = db.Column(db.Integer, unique=True)
    telegram_username = db.Column(db.String(40))
    telegram_name = db.Column(db.String(40))
    number_of_weeks_completed = db.Column(db.Integer)
    last_break_point = db.Column(
        db.DateTime, nullable=False, default=datetime.utcnow()
    )
    is_active_bounter = db.Column(db.Boolean(), default=False)

    def __init__(self, tid, t_name, t_username=""):
        self.telegram_id = tid
        self.telegram_username = t_username
        self.telegram_name = t_name
        self.number_of_weeks_completed = 0
        self.is_active_bounter = False

    

    

    def __str__(self):
        return f'''
<id: {self.telegram_id}, name: {self.telegram_name}
username: {self.telegram_username} active bounter: {self.is_active_bounter} >'''
    
    @staticmethod
    def get_bounter(telegram_id=None, telegram_name=None, telegram_username=None):
        if telegram_id:
            return Bounter.query.filter_by(telegram_id=telegram_id).first()
        if telegram_name:
            return Bounter.query.filter_by(telegram_name=telegram_name).first()
        if telegram_username:
            return Bounter.query.filter_by(telegram_username=telegram_username).first()
        return None
    
    @staticmethod
    def break_point(telegram_id, update_week=False):
        user = Bounter.get_bounter(telegram_id=telegram_id)
        if not user:
            return
        if update_week and user.is_active_bounter:
            last_break_point = user.last_break_point
            this_moment = datetime.utcnow()
            t_delta = this_moment - last_break_point
            #wks = t_delta.days // 6
            wks = (t_delta.seconds / 60) // 2
            user.number_of_weeks_completed += wks
        user.last_break_point = datetime.utcnow()
        return True

    @staticmethod
    def set_is_active_bounter(telegram_id, is_active):
        user = Bounter.get_bounter(telegram_id=telegram_id)
        user.is_active_bounter = is_active

    @staticmethod
    def add_bounter(tid, t_name, t_username=""):
        new_bounter = Bounter(tid, t_name, t_username)
        db_add(new_bounter)
        return new_bounter



class Tasks(BaseModel):
    __Tablename__ = 'Tasks'
    id = db.Column(db.Integer, primary_key=True)
    task_desc = db.Column(db.String(1000), unique=True)

    def __init__(self, task_desc):
        self.task_desc = task_desc


#some functions

def db_commit():
    db.session.commit()

def db_add(obj):
    db.session.add(obj)
